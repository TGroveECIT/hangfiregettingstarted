﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using Microsoft.VisualBasic;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Email;

namespace SerilogConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string email = "tgrove@ecitsoftware.com";
            string mailServer = "mxscan1.mhhosting.dk";
            Serilog.Debugging.SelfLog.Enable(msg => Console.WriteLine(msg));
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File("logs\\myapp.txt", rollingInterval: RollingInterval.Day)
                .WriteTo.Email(new EmailConnectionInfo
                {
                    FromEmail = email,
                    ToEmail = email,
                    MailServer = mailServer,
                    EmailSubject = "Test subject",
                    EnableSsl = true,
                    Port = 587
                }, 
                batchPostingLimit: 1
                )
                .MinimumLevel.Information()
                .CreateLogger();
            try
            {
                Log.Information("Processed {Number} record in {Time} ms", 500, 120);
            }
            finally
            {
                Log.CloseAndFlush();

            }


        }
    }
}
