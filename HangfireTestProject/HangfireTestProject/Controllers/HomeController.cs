﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HangfireTestProject.Models;
using Serilog;


namespace HangfireTestProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
      
        
        [HttpGet]
        public IActionResult Index()
        {
            _logger.LogInformation("Hello from Index");
            return View();
        }

        [HttpPost]
        public ActionResult Index(Message message)
        {
            if (ModelState.IsValid)
            {


                _logger.LogInformation("Hello, Serilog!, from post to Index");
                _logger.LogWarning("Så send dog en email..");
                Hangfire.BackgroundJob.Enqueue(() => Console.WriteLine("Hello from post to index"));

            }

            return RedirectToAction("Index");
        }
        public IActionResult Privacy()
        {
            return View();
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
